﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SearchConeController : MonoBehaviour
{
    private MeshCollider meshCollider;

    private void Start()
    {
        meshCollider = GetComponent<MeshCollider>();
        if (meshCollider == null)
        {
            Debug.LogWarning("Missing mesh collider on search cone on " + transform.parent.parent.name);
        }
        else if (!meshCollider.isTrigger)
        {
            Debug.LogWarning("Mesh collider on " + transform.parent.parent.name + " should be a trigger");
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        IsPlayer isPlayer = other.gameObject.GetComponent<IsPlayer>();
        if (isPlayer)
        {
            // The player is in sight, but are they hiding behind an obstacle?
            // Raycast to find out.
            float distToPlayer = Vector3.Distance(transform.parent.parent.position, isPlayer.transform.position);

            RaycastHit hit;
            if (Physics.Raycast(transform.parent.parent.position, transform.parent.parent.TransformDirection(Vector3.forward), out hit, distToPlayer))
            {
                IsObstacle isObstacle = hit.collider.gameObject.GetComponent<IsObstacle>();
                if (isObstacle)
                {
                    Debug.Log("There's a collider in the way!");
                }
                else
                {
                    OnPlayerCharacterDetected();
                }
            }
            else
            {
                OnPlayerCharacterDetected();
            }
        }
    }

    private void OnPlayerCharacterDetected()
    {
        //Debug.Log("Player character detected!");
        GameStateManager.Instance.ChangeGameState(GameStateManager.eGameState.GameOver);
    }
}
