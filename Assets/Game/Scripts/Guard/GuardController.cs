﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Animator))]
public class GuardController : MonoBehaviour
{
    private Transform initialTransform;

    private NavMeshAgent agent;
    [SerializeField] private Animator guardFSM;
    private Animator guardAnimator;

    void Start()
    {
        initialTransform = this.transform;
        agent = GetComponent<NavMeshAgent>();
        guardAnimator = GetComponent<Animator>();

        GameStateManager.Instance.gameStateChanged.AddListener(OnGameStateChanged);
    }

    private void OnGameStateChanged(GameStateManager.eGameState state)
    {
        switch (state)
        {
            case GameStateManager.eGameState.Spawn:
                this.transform.position = initialTransform.position;
                this.transform.rotation = initialTransform.rotation;

                if (guardFSM)
                {
                    guardFSM.SetBool("MoveToGoal", true);
                }

                agent.enabled = true;
                agent.SetDestination(initialTransform.position);
                break;
            case GameStateManager.eGameState.GameOver:
                guardAnimator.SetFloat("Speed", 0.0f);
                if (guardFSM)
                {
                    guardFSM.SetBool("MoveToGoal", false);
                }

                agent.enabled = false;
                break;
        }
    }
}
