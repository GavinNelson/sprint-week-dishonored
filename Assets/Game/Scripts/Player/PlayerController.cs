﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(PlayerNavPath))]
[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(Collider))]
[RequireComponent(typeof(Rigidbody))]
public class PlayerController : MonoBehaviour
{
    public LayerMask blinkLayerMask;

    public int maxBlinks = 3;
    private float numBlinksLeft;

    public float blinkSpeed = 8.0f;

    private SkinnedMeshRenderer[] meshRenderers;
    private PlayerNavPath playerNavPath;
    private NavMeshAgent playerAgent;
    private Collider playerCollider;
    private Rigidbody playerRB;
    [SerializeField]private GameObject blinkParticlesPrefab = null;
    private ParticleSystem[] blinkParticles;

    private PlayerNavPathPoint currentBlinkGoalPoint = null;
    private Vector3 currentGoalPos = Vector3.zero;

    void Start()
    {
        numBlinksLeft = maxBlinks;

        meshRenderers = GetComponentsInChildren<SkinnedMeshRenderer>();
        playerNavPath = GetComponent<PlayerNavPath>();
        playerAgent = GetComponent<NavMeshAgent>();
        playerCollider = GetComponent<Collider>();
        playerRB = GetComponent<Rigidbody>();

        playerAgent.SetDestination(transform.position);

        if (blinkParticlesPrefab != null)
        {
            blinkParticles = blinkParticlesPrefab.GetComponentsInChildren<ParticleSystem>();
            HideBlinkParticles();
        }

        GameStateManager.Instance.gameStateChanged.AddListener(OnGameStateChanged);
    }

    private void OnGameStateChanged(GameStateManager.eGameState state)
    {
        switch (state)
        {
            case GameStateManager.eGameState.GameOver:
                Destroy(this.gameObject);
                break;
        }
    }

    #region Particle Controls
    private void ShowBlinkParticles()
    {
        foreach (ParticleSystem particle in blinkParticles)
        {
            particle.gameObject.SetActive(true);
            particle.Play();
        }
    }

    private void HideBlinkParticles()
    {
        foreach (ParticleSystem particle in blinkParticles)
        {
            particle.gameObject.SetActive(false);
            particle.Stop();
        }
    }
    #endregion

    #region Mesh Controls
    private void HidePlayerMesh()
    {
        foreach (SkinnedMeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.enabled = false;
        }
    }

    private void ShowPlayerMesh()
    {
        foreach (SkinnedMeshRenderer meshRenderer in meshRenderers)
        {
            meshRenderer.enabled = true;
        }
    }
    #endregion

    #region Player Movement Functions
    public void AddBlink()
    {
        numBlinksLeft = (numBlinksLeft + 1 <= maxBlinks) ? numBlinksLeft + 1 : maxBlinks;
    }

    private void DoBlink(PlayerNavPathPoint goalPoint)
    {
        HidePlayerMesh();
        ShowBlinkParticles();

        currentBlinkGoalPoint = goalPoint;
        currentGoalPos = new Vector3(currentBlinkGoalPoint.GetPosition().x, transform.position.y, currentBlinkGoalPoint.GetPosition().z);

        playerAgent.enabled = false;
        playerCollider.enabled = false;

        numBlinksLeft = (numBlinksLeft - 1 >= 0) ? numBlinksLeft - 1 : 0;

        GameStateManager.Instance.ChangeGameState(GameStateManager.eGameState.PlayerMoving);
    }

    public void Blink(PlayerNavPathPoint goalPoint)
    {
        if (!playerNavPath.IsEvaluatingPath() && numBlinksLeft > 0)
        {
            // Check if there is a blinkproof obstacle in the way
            Vector3 playerXZ = new Vector3(playerAgent.destination.x, transform.position.y, playerAgent.destination.z);
            Vector3 goalXZ = new Vector3(goalPoint.GetPosition().x, transform.position.y, goalPoint.GetPosition().z);

            float playerToGoal = Vector3.Distance(playerXZ, goalXZ);
            Vector3 dir = (goalXZ - playerXZ);

            RaycastHit hit;
            if (Physics.Raycast(transform.position, dir.normalized, out hit, playerToGoal, blinkLayerMask))
            {
                IsBlinkproofObstacle isBlinkproofObstacle = hit.collider.gameObject.GetComponent<IsBlinkproofObstacle>();
                if (isBlinkproofObstacle)
                {
                    return;
                }
                else
                {
                    DoBlink(goalPoint);
                }
            }
            else
            {
                DoBlink(goalPoint);
            }
        }
    }

    public void AddNavPoint(PlayerNavPathPoint goalPoint)
    {
        Vector3 playerXZ = new Vector3(playerAgent.destination.x, transform.position.y, playerAgent.destination.z);
        Vector3 goalXZ = new Vector3(goalPoint.GetPosition().x, transform.position.y, goalPoint.GetPosition().z);

        float playerToGoal = Vector3.Distance(playerXZ, goalXZ);
        Vector3 dir = (goalXZ - playerXZ);

        RaycastHit hit;
        if (Physics.Raycast(transform.position, dir.normalized, out hit, playerToGoal, blinkLayerMask))
        {
            IsBlinkproofObstacle isBlinkproofObstacle = hit.collider.gameObject.GetComponent<IsBlinkproofObstacle>();
            if (isBlinkproofObstacle)
            {
                return;
            }
            else
            {
                playerNavPath.AddNavPoint(goalPoint);
            }
        }
        else
        {
            playerNavPath.AddNavPoint(goalPoint);
        }
    }


    public void Move()
    {
        if (playerNavPath.pathPoints.Count > 0)
        {
            playerNavPath.StartNavPath();
            GameStateManager.Instance.ChangeGameState(GameStateManager.eGameState.PlayerMoving);
        }
    }
    #endregion

    void Update()
    {
        if (currentBlinkGoalPoint != null)
        {
            if (transform.position == currentGoalPos)
            {
                HideBlinkParticles();
                ShowPlayerMesh();

                playerAgent.enabled = true;
                playerAgent.SetDestination(currentGoalPos);

                currentBlinkGoalPoint.OnPointReached();
                currentBlinkGoalPoint = null;
                currentGoalPos = Vector3.zero;

                playerCollider.enabled = true;

                GameStateManager.Instance.ChangeGameState(GameStateManager.eGameState.PlayerMovementSelection);
            }
            else
            {
                transform.position = Vector3.Lerp(transform.position, currentGoalPos, Time.deltaTime * blinkSpeed);
            }
        }
    }


}
