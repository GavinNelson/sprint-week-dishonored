﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class PickupBase : MonoBehaviour
{
    private void OnTriggerEnter(Collider collider)
    {
        OnPickupCollected(collider);
    }

    protected abstract void OnPickupCollected(Collider collider);
}
