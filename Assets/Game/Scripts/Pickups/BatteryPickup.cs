﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BatteryPickup : PickupBase
{
    private MeshRenderer[] renderers;
    private ParticleSystem[] particles;

    private void Start()
    {
        renderers = GetComponentsInChildren<MeshRenderer>();
        particles = GetComponentsInChildren<ParticleSystem>();
    }

    private IEnumerator DestroySelf(float destroyTime)
    {
        yield return new WaitForSeconds(destroyTime);
        Destroy(this.gameObject);
    }

    protected override void OnPickupCollected(Collider collider)
    {
        PlayerController player = collider.gameObject.GetComponent<PlayerController>();
        if (player)
        {
            player.AddBlink();

            if (particles.Length > 0)
            {
                foreach (MeshRenderer renderer in renderers)
                {
                    renderer.enabled = false;
                }

                foreach (ParticleSystem particle in particles)
                {
                    particle.Play();
                }

                StartCoroutine(DestroySelf(1.0f));
            }
            else
            {
                StartCoroutine(DestroySelf(0.0f));
            }
        }
    }
}
