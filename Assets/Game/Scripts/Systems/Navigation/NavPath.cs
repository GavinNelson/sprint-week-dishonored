﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface INavPathPoint
{
    void OnPointReached();
    Vector3 GetPosition();
}

public abstract class NavPath : MonoBehaviour
{
    protected int currentNavPointIndex = -1;

    public abstract INavPathPoint GetCurrentGoalPoint();
    public abstract INavPathPoint GetNextGoalPoint();
}
