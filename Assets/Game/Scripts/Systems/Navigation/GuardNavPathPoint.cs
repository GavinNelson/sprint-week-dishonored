﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GuardNavPathPoint : NavPathPoint, INavPathPoint
{
    // Whether or not this path point should trigger the guard to idle
    public bool shouldTriggerIdle = false;

    // The amount of time to idle for
    public float idleTime = 0.5f;
}
