﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuardNavPath : NavPath
{
    public float pointWidth = 0.5f;
    public Color pointColor = Color.cyan;

    // Whether or not the guard should circle back to the first point
    // instead of going backwards at the end of the path
    public bool isCyclePath = false;

    public GuardNavPathPoint[] pathPoints;
    public bool isGoingBackwards = false;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = pointColor;

        if (pathPoints != null)
        {
            foreach (NavPathPoint point in pathPoints)
            {
                Gizmos.DrawSphere(point.position, pointWidth);
            }
        }
    }

    public override INavPathPoint GetCurrentGoalPoint()
    {
        if (pathPoints != null && pathPoints.Length > 0)
        {
            return pathPoints[currentNavPointIndex];
        }
        else
        {
            return null;
        }
    }

    public override INavPathPoint GetNextGoalPoint()
    {
        if (pathPoints != null && pathPoints.Length > 0)
        {
            if (isCyclePath)
            {
                currentNavPointIndex = ((currentNavPointIndex + 1) > pathPoints.Length - 1) ? 0 : (currentNavPointIndex + 1);
            }
            else
            {
                if (isGoingBackwards)
                {
                    --currentNavPointIndex;

                    if (currentNavPointIndex < 0)
                    {
                        currentNavPointIndex = 0;
                        isGoingBackwards = false;
                    }
                }
                else
                {
                    ++currentNavPointIndex;

                    if (currentNavPointIndex > pathPoints.Length - 1)
                    {
                        currentNavPointIndex = pathPoints.Length - 1;
                        isGoingBackwards = true;
                    }
                }
            }

            return pathPoints[currentNavPointIndex];
        }
        else
        {
            return null;
        }
    }
}
