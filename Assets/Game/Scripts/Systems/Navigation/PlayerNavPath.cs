﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNavPath : NavPath
{
    public List<PlayerNavPathPoint> pathPoints = new List<PlayerNavPathPoint>();

    private bool isEvaluatingPath = false;

    public bool IsEvaluatingPath()
    {
        return isEvaluatingPath;
    }

    public void AddNavPoint(PlayerNavPathPoint navPoint)
    {
        if (!pathPoints.Contains(navPoint))
        {
            pathPoints.Add(navPoint);
        }
    }

    public void RemoveNavPoint(PlayerNavPathPoint navPoint)
    {
        pathPoints.Remove(navPoint);
    }

    public void StartNavPath()
    {
        if (pathPoints.Count > 0)
        {
            Animator playerFSM = transform.Find("BrainFSM").GetComponent<Animator>();
            if (playerFSM)
            {
                playerFSM.SetBool("MoveToGoal", true);
                isEvaluatingPath = true;
            }
        }
    }

    public override INavPathPoint GetCurrentGoalPoint()
    {
        if (pathPoints != null && pathPoints.Count > 0)
        {
            if (currentNavPointIndex < 0)
            {
                ++currentNavPointIndex;
            }

            return pathPoints[currentNavPointIndex];
        }
        else
        {
            return null;
        }
    }

    public override INavPathPoint GetNextGoalPoint()
    {
        if (pathPoints != null && pathPoints.Count > 0)
        {
            ++currentNavPointIndex;

            if (currentNavPointIndex > pathPoints.Count - 1)
            {
                pathPoints.Clear();
                currentNavPointIndex = -1;
                isEvaluatingPath = false;

                GameStateManager.Instance.ChangeGameState(GameStateManager.eGameState.PlayerMovementSelection);

                return null;
            }
            else
            {
                return pathPoints[currentNavPointIndex];
            }
        }
        else
        {
            return null;
        }
    }
}
