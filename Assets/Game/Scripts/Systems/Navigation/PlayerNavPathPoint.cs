﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerNavPathPoint : MonoBehaviour, INavPathPoint
{
    public UnityEvent onPointReachedCallback;

    private void Start()
    {
    }

    private void Update()
    {   
    }

    public void OnPointReached()
    {
        //Debug.Log("Point reached!");

        if (onPointReachedCallback != null)
        {
            onPointReachedCallback.Invoke();
        }
    }

    public Vector3 GetPosition()
    {
        return transform.position;
    }
}
