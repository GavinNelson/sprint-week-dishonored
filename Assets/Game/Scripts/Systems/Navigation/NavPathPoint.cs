﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class NavPathPoint : INavPathPoint
{
    public Vector3 position = Vector3.zero;
    public UnityEvent onPointReachedCalback;

    public Vector3 GetPosition()
    {
        return position;
    }

    public void OnPointReached()
    {
        //Debug.Log("Point reached!");

        if (onPointReachedCalback != null)
        {
            onPointReachedCalback.Invoke();
        }
    }
}
