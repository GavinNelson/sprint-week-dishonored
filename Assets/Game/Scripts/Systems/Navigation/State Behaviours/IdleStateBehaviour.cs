﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IdleStateBehaviour : StateMachineBehaviour
{
    private GameObject characterRoot;
    private NavMeshAgent agent;
    private GuardNavPath navPath;
    private GuardNavPathPoint currentNavPoint;
    private IsGuard isGuard = null;

    private float currentIdleTime = 0.0f;

    public override void OnStateEnter(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        characterRoot = fsm.gameObject.transform.parent.gameObject;
        agent = characterRoot.GetComponentInParent<NavMeshAgent>();
        navPath = characterRoot.GetComponentInParent<GuardNavPath>();

        isGuard = characterRoot.GetComponent<IsGuard>();

        if (navPath != null)
        {
            currentNavPoint = (GuardNavPathPoint)navPath.GetCurrentGoalPoint();
        }
        else
        {
            Debug.LogWarning(fsm.gameObject.name + " does not have a navigation path");
        }
    }

    public override void OnStateUpdate(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (isGuard && currentNavPoint != null && !GameStateManager.Instance.IsGameOver)
        {
            currentIdleTime += Time.deltaTime;

            if (currentIdleTime >= currentNavPoint.idleTime)
            {
                currentIdleTime = 0.0f;
                fsm.SetBool("MoveToGoal", true);
                //currentNavPoint = (GuardNavPathPoint)navPath.GetNextGoalPoint();
                //if (currentNavPoint != null)
                //{
                //    agent.SetDestination(currentNavPoint.GetPosition());
                //    fsm.SetBool("MoveToGoal", true);
                //}
            }
        }
    }

    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        currentNavPoint = null;
        currentIdleTime = 0.0f;
    }
}
