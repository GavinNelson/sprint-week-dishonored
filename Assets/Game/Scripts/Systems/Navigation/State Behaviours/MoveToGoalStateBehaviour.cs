﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

public class MoveToGoalStateBehaviour : StateMachineBehaviour
{
    public float angularDampeningTime = 5.0f;
    public float deadZone = 10.0f;

    private GameObject characterRoot;
    private NavPath navPath;
    private NavMeshAgent agent;
    private Animator characterAnimator;
    private Transform characterTransform;

    private AnimationListener animationListener;
    private UnityAction onAnimatorMoveCallback;

    private IsGuard isGuard = null;

    private void OnAnimatorMove()
    {
        agent.velocity = characterAnimator.deltaPosition / Time.deltaTime;
    }

    public override void OnStateEnter(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        characterRoot = fsm.gameObject.transform.parent.gameObject;
        navPath = characterRoot.GetComponent<NavPath>();
        characterAnimator = characterRoot.GetComponent<Animator>();

        isGuard = characterRoot.GetComponent<IsGuard>();

        animationListener = characterRoot.GetComponent<AnimationListener>();
        onAnimatorMoveCallback = new UnityAction(OnAnimatorMove);
        animationListener.AddAnimatorMoveListener(onAnimatorMoveCallback);

        agent = characterRoot.GetComponent<NavMeshAgent>();
        characterTransform = characterRoot.GetComponent<Transform>();

        INavPathPoint currentNavPoint = navPath.GetNextGoalPoint();
        if (currentNavPoint != null && agent.isOnNavMesh)
        {
            Vector3 currentGoalPos = currentNavPoint.GetPosition();
            agent.SetDestination(currentGoalPos);
        }
    }

    public override void OnStateUpdate(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!GameStateManager.Instance.IsGameOver)
        {
            if (agent.desiredVelocity != Vector3.zero)
            {
                float speed = Vector3.Project(agent.desiredVelocity, characterTransform.forward).magnitude * agent.speed;
                characterAnimator.SetFloat("Speed", speed);

                float angle = Vector3.Angle(characterTransform.forward, agent.desiredVelocity);
                if (Mathf.Abs(angle) <= deadZone)
                {
                    // Don't even bother with lerping the transform
                    characterTransform.LookAt(characterTransform.position + agent.desiredVelocity);
                }
                else
                {
                    characterTransform.rotation = Quaternion.Lerp(characterTransform.rotation, Quaternion.LookRotation(agent.desiredVelocity), Time.deltaTime * angularDampeningTime);
                }
            }
            else
            {
                INavPathPoint currentGoalPoint = navPath.GetCurrentGoalPoint();
                if (currentGoalPoint != null)
                {
                    currentGoalPoint.OnPointReached();

                    if (isGuard && ((GuardNavPathPoint)currentGoalPoint).shouldTriggerIdle)
                    {
                        characterAnimator.SetFloat("Speed", 0.0f);
                        fsm.SetBool("MoveToGoal", false);

                    }
                    else
                    {
                        INavPathPoint nextGoalPoint = navPath.GetNextGoalPoint();
                        if (nextGoalPoint != null)
                        {
                            agent.SetDestination(nextGoalPoint.GetPosition());
                        }
                        else
                        {
                            characterAnimator.SetFloat("Speed", 0.0f);
                            fsm.SetBool("MoveToGoal", false);
                        }
                    }
                }
            }
        }
    }

    public override void OnStateExit(Animator fsm, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animationListener.RemoveAnimatorMoveListener(onAnimatorMoveCallback);
    }
}
