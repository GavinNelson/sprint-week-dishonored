﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public GameObject fingerTracker;
    public GameObject fingerCollider;

    public float distanceFromCamera;

    public Vector3 touchPositionWorld;


    void Awake()
    {
      
    }


    void Update()
    {
        // When the screen is touched
        if (Input.touchCount > 0)
        {
            // Takes the first screen touch
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                Vector2 touchPoint = touch.position;

                // Convert the touch position to world units for the tracker's position
                touchPositionWorld = Camera.main.ScreenToWorldPoint(new Vector3(touchPoint.x, touchPoint.y, distanceFromCamera));

                // Instantiates the finger tracker sprite object on touch
                //Instantiate(fingerCollider, trackerPosition, transform.rotation);
                Instantiate(fingerTracker, touchPositionWorld, Quaternion.LookRotation(-Camera.main.transform.forward));
            }

            if (touch.phase == TouchPhase.Moved)
            {
                Vector2 touchPoint = touch.position;

                // Convert the touch position to world units for the tracker's position
                touchPositionWorld = Camera.main.ScreenToWorldPoint(new Vector3(touchPoint.x, touchPoint.y, distanceFromCamera));
            }

            if (touch.phase == TouchPhase.Ended)
            {
                GameObject clone = GameObject.FindGameObjectWithTag("Tracker");
                Destroy(clone);
            }
        }
    }
}
