﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TheInputManager : Singleton<TheInputManager>
{
    public float tapTimeTreshold = 0.4f;
    public float distanceFromCamera = 9;

    public GameObject fingerTracker;

    public Vector3 touchPositionWorld;
    LayerMask layerMask;

    private float currentTapTime = 0.0f;
    private bool isFirstMove = true;

    [SerializeField]
    private GameObject pathNodesRoot = null;
    private PlayerNavPathPoint[] pathNodes;

    private void Awake()
    {
        layerMask = LayerMask.GetMask("Node", "CheckPoint");

        if (pathNodesRoot)
        {
            pathNodes = pathNodesRoot.GetComponentsInChildren<PlayerNavPathPoint>();

            if (pathNodes.Length > 0)
            {
                HidePathNodes();
            }
            else
            {
                Debug.LogWarning("Error: Missing nodes references on nodes root");
            }
        }
        else
        {
            Debug.LogWarning("Error: Missing path nodes root reference on input manager");
        }

        GameStateManager.Instance.gameStateChanged.AddListener(OnGameStateChanged);
    }

    private void Update()
    {
        if (GameStateManager.Instance.currentState == GameStateManager.eGameState.PlayerMovementSelection)
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                currentTapTime += touch.deltaTime;

                if (touch.tapCount >= 2)
                {
                    Blink(touch);
                }
                else if (currentTapTime >= tapTimeTreshold)
                {
                    if (isFirstMove)
                    {
                        isFirstMove = false;

                        touchPositionWorld = GetTouchWorldPosition(touch);
                        Instantiate(fingerTracker, touchPositionWorld, fingerTracker.transform.rotation);
                    }

                    Move(touch);
                }

            }
            else
            {
                currentTapTime = 0.0f;
                isFirstMove = true;
            }
        }
    }

    private void Blink(Touch touch)
    {
        if (touch.phase == TouchPhase.Began)
        {
            touchPositionWorld = GetTouchWorldPosition(touch);

            RaycastHit[] hits = Physics.RaycastAll(touchPositionWorld, Vector3.down, layerMask);
            foreach (RaycastHit hit in hits)
            {
                PlayerNavPathPoint navPathPoint = hit.collider.gameObject.GetComponent<PlayerNavPathPoint>();
                if (navPathPoint)
                {
                    PlayerController player = GameStateManager.Instance.GetPlayerInstance();
                    if (player)
                    {
                        player.Blink(navPathPoint);
                    }
                }
            }
        }
    }

    private void Move(Touch touch)
    {
        if (touch.phase == TouchPhase.Moved)
        {
            touchPositionWorld = GetTouchWorldPosition(touch);

            RaycastHit[] hits = Physics.RaycastAll(touchPositionWorld, Vector3.down, layerMask);
            foreach (RaycastHit hit in hits)
            {
                PlayerNavPathPoint pathPoint = hit.collider.gameObject.GetComponent<PlayerNavPathPoint>();
                if (pathPoint)
                {
                    PlayerController player = GameStateManager.Instance.GetPlayerInstance();
                    if (player)
                    {
                        player.AddNavPoint(pathPoint);
                    }
                }
            }
        }

        if (touch.phase == TouchPhase.Ended)
        {
            GameObject clone = GameObject.FindGameObjectWithTag("Tracker");
            Destroy(clone);

            PlayerController player = GameStateManager.Instance.GetPlayerInstance();
            if (player)
            {
                player.Move();
            }
        }
    }

    private Vector3 GetTouchWorldPosition(Touch touch)
    {
        Vector2 touchPositionScreen = touch.position;

        touchPositionWorld = Camera.main.ScreenToWorldPoint(new Vector3(touchPositionScreen.x, touchPositionScreen.y, distanceFromCamera));

        return touchPositionWorld;
    }

    private void OnGameStateChanged(GameStateManager.eGameState gameState)
    {
        switch (gameState)
        {
            case GameStateManager.eGameState.PlayerMovementSelection:
                ShowPathNodes();
                break;
            case GameStateManager.eGameState.PlayerMoving:
                HidePathNodes();
                break;
        }
    }

    private void HidePathNodes()
    {
        foreach (PlayerNavPathPoint node in pathNodes)
        {
            MeshRenderer renderer = node.GetComponent<MeshRenderer>();
            if (renderer)
            {
                renderer.enabled = false;
            }
        }
    }

    private void ShowPathNodes()
    {
        foreach (PlayerNavPathPoint node in pathNodes)
        {
            MeshRenderer renderer = node.GetComponent<MeshRenderer>();
            if (renderer)
            {
                renderer.enabled = true;
            }
        }
    }
}
