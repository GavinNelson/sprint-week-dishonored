﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameStateManager : Singleton<GameStateManager>
{
    public GameObject playerPrefab = null;
    public Transform playerSpawnPos;

    private PlayerController currentPlayerInstance = null;

    private bool isGameOver = false;
    public bool IsGameOver
    {
        get
        {
            return isGameOver;
        }
    }

    public enum eGameState
    {
        Spawn,
        PlayerMovementSelection,
        PlayerMoving,
        GameOver,
        Victory,
    }
    public eGameState currentState = eGameState.Spawn;

    [System.Serializable]
    public class GameStateChangedEvent : UnityEvent<GameStateManager.eGameState> { }
    public GameStateChangedEvent gameStateChanged;

    public void ChangeGameState(eGameState state)
    {
        currentState = state;
        switch (currentState)
        {
            case eGameState.Spawn:
                SpawnPlayerCharacter();
                isGameOver = false;
                //Time.timeScale = 1.0f;
                Invoke("SwitchToFirstMove", 1.5f);
                break;
            case eGameState.PlayerMovementSelection:
                break;
            case eGameState.PlayerMoving:
                break;
            case eGameState.GameOver:
                isGameOver = true;
                //Time.timeScale = 0.0f;
                break;
            case eGameState.Victory:
                break;
        }

        gameStateChanged.Invoke(currentState);
    }

    public PlayerController GetPlayerInstance()
    {
        if (currentPlayerInstance != null)
        {
            return currentPlayerInstance;
        }
        else
        {
            return null;
        }
    }

    private void SwitchToFirstMove()
    {
        ChangeGameState(eGameState.PlayerMovementSelection);
    }

    private void SpawnPlayerCharacter()
    {
        if (currentPlayerInstance)
        {
            Destroy(currentPlayerInstance.gameObject);
        }

        currentPlayerInstance = Instantiate(playerPrefab, playerSpawnPos.position, playerSpawnPos.rotation).GetComponent<PlayerController>();
    }

    private void Awake()
    {
        ChangeGameState(eGameState.Spawn);
    }

    private void Update()
    {
        // Potentially have a timer for delays between states here?
    }
}
