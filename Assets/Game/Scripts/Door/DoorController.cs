﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    private bool isOpen = false;
    public bool IsOpen
    {
        get
        {
            return isOpen;
        }
    }

    public void OpenDoor()
    {
        Destroy(this.gameObject);
    }
}
